import express, { NextFunction } from 'express';
import morgan from 'morgan';
import * as mongoose from 'mongoose';
import { router as placesRoute } from './app/routes/places-routes';
import { errorHandler, noRouteFound } from './app/middleware/errorHandler';
import { router as userRoute } from './app/routes/user-routes';

const app = express();
mongoose
  .connect(
    'mongodb+srv://wtf:fuckyou1@mern.7skwlml.mongodb.net/new?retryWrites=true&w=majority'
  )
  .then(() => console.log('connected successfully'))
  .catch((err) => console.log(err.message));

app.use(morgan('dev'));
app.use(express.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
  next();
});

app.use('/uploads/images/', express.static(`${__dirname}/uploads/images`));

app.use('/api/places', placesRoute);
app.use('/api/users', userRoute);

app.use(noRouteFound);
app.use(errorHandler);
app.listen(5000, () => {
  console.log('sever is running on port 5000');
});
