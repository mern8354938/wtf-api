import { NextFunction, Request, Response } from 'express';
import { HttpError } from '../modal/http-error';
import { validationResult } from 'express-validator';
import { errorMessage } from '../helper/errorMsg';
import { getCordsForAddress } from '../utils/location';
import PlaceSchema, { IPlace } from '../modal/place';
import UserSchema, { IUser } from '../modal/user';
import { startSession, ClientSession } from 'mongoose';
import fs from 'fs';
export const getPlaceById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const placeId = req.params.pid;
  let place;
  try {
    place = await PlaceSchema.findById(placeId);
  } catch {
    return next(
      new HttpError('something went wrong,could not find place', 500)
    );
  }
  if (!place) {
    return next(new HttpError('Place ID not found', 404));
  }
  res.status(200).json({
    status: 'success',
    place: place.toObject(),
  });
};

export const getPlacesByUserId = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.params.uid;
  try {
    const places = await PlaceSchema.find({ creator: { $eq: userId } });
    if (!places || places.length === 0) {
      return next(new HttpError('no places found for user ID', 404));
    }
    res.status(200).json({
      status: 'success',
      places: places.map((doc) => doc.toObject()),
    });
  } catch (e) {
    return next(
      new HttpError('something went wrong,could not find place', 500)
    );
  }
};

export const createPlace = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const error = validationResult(req);

  if (!error.isEmpty()) {
    const errorMsg = errorMessage(error.array());
    next(new HttpError(errorMsg, 422));
  }
  const { title, description, address, creator } = req.body;
  let coordinates;
  try {
    coordinates = await getCordsForAddress(address);
    if (coordinates instanceof HttpError) return next(coordinates);
  } catch (e) {
    return next(e);
  }

  const createdPlace: IPlace = new PlaceSchema({
    title,
    description,
    address,
    location: coordinates,
    image: req.file?.path,
    creator,
  });
  let user: IUser | null;
  try {
    user = await UserSchema.findById(creator);
  } catch {
    return next(new HttpError('creating place failed', 500));
  }
  if (!user) {
    return next(new HttpError('could not find user for provided id', 404));
  }
  try {
    const session: ClientSession = await startSession();
    session.startTransaction();
    await createdPlace.save({ session });
    user.places.push(createdPlace._id);
    await user.save({ session });
    await session.commitTransaction();
    await session.endSession();
  } catch (e) {
    const error = new HttpError('Creating place failed', 500);
    return next(error);
  }
  res.status(201).json({ status: 'success', data: createdPlace });
};

export const updatePlaceById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const error = validationResult(req);
  if (!error.isEmpty()) {
    const errorMsg = errorMessage(error.array());
    return next(new HttpError(errorMsg, 422));
  }
  const { title, description } = req.body;
  const id = req.params.pid;
  const update = { title, description };
  let place;
  try {
    place = await PlaceSchema.findByIdAndUpdate(id, update, { new: true });
  } catch {
    return next(new HttpError('something went wrong', 500));
  }
  res.status(200).json({ status: 'success', place: place?.toObject() });
};

export const deletePlace = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.pid;
  let imagePath;
  try {
    const place = await PlaceSchema.findById(id).populate('creator');
    if (place?.image) {
      imagePath = place.image;
    }
    if (!place) return next(new HttpError('Invalid Id', 400));
    const session = await startSession();
    session.startTransaction();
    await UserSchema.updateOne(
      { _id: place.creator },
      { $pull: { places: id } },
      { session }
    );
    await place.deleteOne({ session });
    await session.commitTransaction();
    await session.endSession();
    if (imagePath) {
      fs.unlink(imagePath, (err) => {
        console.log(err);
      });
    }

    res.status(200).json({ message: 'place deleted successfully' });
  } catch {
    return next(new HttpError('Failed to delete place', 500));
  }
};
