import { NextFunction, Request, Response } from 'express';
import { HttpError } from '../modal/http-error';
import { validationResult } from 'express-validator';
import { errorMessage } from '../helper/errorMsg';
import UserSchema from '../modal/user';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

export const getAllUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const users = await UserSchema.find({}, '-password');
    res.status(200).json({
      message: 'success',
      length: users.length,
      users: users.map((doc) => doc.toObject()),
    });
  } catch {
    return next(new HttpError('server failed to find any users', 500));
  }
};

export const loginUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, password } = req.body;
  let existingUser;
  try {
    existingUser = await UserSchema.findOne({ email: { $eq: email } });
  } catch {
    return next(new HttpError('Login failed', 500));
  }
  if (!existingUser) {
    return next(new HttpError('user not exist', 401));
  }
  let isValidPassword = false;
  try {
    isValidPassword = await bcrypt.compare(password, existingUser.password);
  } catch (error) {
    return next(new HttpError('server error', 500));
  }
  if (!isValidPassword) return next(new HttpError('invalid credentials', 401));
  let token;
  try {
    token = jwt.sign(
      { userId: existingUser.id, email: existingUser.email },
      'the_wtf_api_is_great',
      {
        expiresIn: '1hr',
      }
    );
  } catch {
    return next(new HttpError('login failed failed, Please try again', 500));
  }
  res.status(200).json({
    status: 'success',
    user: { id: existingUser.id, email: existingUser.email, token },
  });
};

export const signUpUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const error = validationResult(req);
  if (!error.isEmpty()) {
    const errorMsg = errorMessage(error.array());
    return next(new HttpError(errorMsg, 422));
  }
  const { email, password, name } = req.body;
  let existingUser;
  try {
    existingUser = await UserSchema.findOne({ email: { $eq: email } });
  } catch {
    return next(new HttpError('Sing up failed', 500));
  }
  if (existingUser) {
    return next(new HttpError('User email exists already', 422));
  }
  let hashPassword;
  try {
    hashPassword = await bcrypt.hash(password, 12);
  } catch (err) {
    return next(new HttpError('internal server error', 500));
  }
  const createdUser = new UserSchema({
    name,
    email,
    password: hashPassword,
    image: req.file?.path,
    places: [],
  });
  try {
    await createdUser.save();
  } catch {
    return next(new HttpError('Signing up failed, Please try again', 500));
  }
  let token;
  try {
    token = jwt.sign(
      { userId: createdUser.id, email: createdUser.email },
      'the_wtf_api_is_great',
      { expiresIn: '1hr' }
    );
  } catch {
    return next(new HttpError('Signing up failed, Please try again', 500));
  }
  res.status(200).json({
    status: 'success',
    user: { userId: createdUser.id, email: createdUser.email, token: token },
  });
};
