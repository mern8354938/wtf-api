import { Place } from '../helper/int';

export const place: Place[] = [
  {
    id: 'p1',
    title: 'First Place',
    description: 'This is the first place',
    location: {
      lat: 12.34,
      lon: 56.78,
    },
    address: '123 Main St, City',
    creator: 'u2',
  },
  {
    id: 'p2',
    title: 'Second Place',
    description: 'This is the second place',
    location: {
      lat: 98.76,
      lon: 54.32,
    },
    address: '456 Elm St, City',
    creator: 'u3',
  },
];

export const users = [
  {
    id: 'u1',
    name: 'John Doe',
    email: 'johndoe@example.com',
    password: 'password123',
  },
  {
    id: 'u2',
    name: 'Jane Smith',
    email: 'janesmith@example.com',
    password: 'hello123',
  },
  {
    id: 'u3',
    name: 'Alice Johnson',
    email: 'alicejohnson@example.com',
    password: 'qwerty456',
  },
];
