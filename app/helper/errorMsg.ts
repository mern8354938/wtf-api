import { ErrorMsgVal } from "./int";

export const errorMessage = (error: ErrorMsgVal[]) =>
  error.map((er) => er.msg).join(", ");
