import { NextFunction, Request, Response } from 'express';

export interface ReqRes {
  req: Request;
  res: Response;
}

export interface MiddleWare extends ReqRes {
  next: NextFunction;
}

export interface RouterHandler {
  (req: Request, res: Response, next: NextFunction): void;
}
export interface Place {
  id: string;
  title: string;
  description: string;
  location: {
    lat: number;
    lon: number;
  };
  address: string;
  creator: string;
}
export interface ErrorMsgVal {
  msg: string;
}
