import { NextFunction, Request, Response } from 'express';
import { HttpError } from '../modal/http-error';
import jwt, { JwtPayload } from 'jsonwebtoken';
interface Token {
  userId: string;
}

export default (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(' ').at(1);
    if (!token) return next(new HttpError('invalid token', 401));
    const decodedToken = jwt.verify(
      token,
      'the_wtf_api_is_great'
    ) as JwtPayload;
    (req as any).userData = { userId: decodedToken.userId };
    next();
  } catch {
    return next(new HttpError('invalid token', 401));
  }
};
