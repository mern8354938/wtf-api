import { ErrorRequestHandler, NextFunction, Request, Response } from 'express';
import fs from 'fs';
import { HttpError } from '../modal/http-error';
export const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  if (req.file) {
    fs.unlink(req.file.path, (error) => {
      res
        .status(err.status || 500)
        .json({ message: error?.message || 'internal server error' });
    });
  }
  if (res.headersSent) return next(err);
  res
    .status(err.status || 500)
    .json({ message: err.message || 'internal server error' });
};

export const noRouteFound = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const err = new HttpError('Route not found', 404);
  next(err);
};
