import multer from 'multer';
import { v4 as uui } from 'uuid';
interface MimeTypes {
  [key: string]: string;
}
const MIME_TYPE_MAP: MimeTypes = {
  'image/png': 'png',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg',
};
const fileUpload = multer({
  limits: { fileSize: 1024 * 1024 },
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads/images');
    },
    filename: (req, file, cb) => {
      const ext = MIME_TYPE_MAP[file.mimetype];
      cb(null, `${uui()}.${ext}`);
    },
  }),
  fileFilter: (req, file, cb) => {
    const isValid = !!MIME_TYPE_MAP[file.mimetype];
    const error = isValid ? null : new Error('Invalid MIME type');
    cb(error as null, isValid);
  },
});
export default fileUpload;
