import { check } from "express-validator";

export const placeValidator = () => [
  check("title").notEmpty().withMessage("Invalid Title"),
  check("description")
    .isLength({ min: 5, max: 40 })
    .withMessage("Description must have least 5 chars and maximum 40 chars"),
  check("address").notEmpty().withMessage("Invalid Adress"),
];

export const placeUpdateValidator = () => [
  check("title").notEmpty().withMessage("Invalid Title"),
  check("description")
    .isLength({ min: 5, max: 40 })
    .withMessage("Description must have least 5 chars and maximum 40 chars"),
];

export const userSignupValidator = () => [
  check("name")
    .notEmpty({ ignore_whitespace: true })
    .withMessage("Name field is required.")
    .isLength({ min: 3, max: 22 })
    .withMessage("Name must be between 3 and 22 characters."),
  check("email").normalizeEmail().isEmail().withMessage("Email is not valid"),
  check("password")
    .notEmpty()
    .withMessage("password field is required")
    .isStrongPassword({
      minLength: 6,
      minLowercase: 2,
      minUppercase: 2,
      minNumbers: 2,
      minSymbols: 2,
    })
    .withMessage("Password must be strong."),
];
