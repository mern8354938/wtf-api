import { PathLike } from 'fs';
import { model, Schema, Document, Types } from 'mongoose';

export interface IPlace extends Document {
  title: string;
  description: string;
  image: PathLike;
  address: string;
  location?: {
    lat: number;
    lon: number;
  };
  creator: Schema.Types.ObjectId;
}

const PlaceSchema = new Schema<IPlace>(
  {
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    address: { type: String, required: true },
    location: {
      lat: { type: Number, required: true },
      lon: { type: Number, required: true },
    },
    creator: { type: Types.ObjectId, required: true, ref: 'UserSchema' },
  },
  {
    toObject: { getters: true, virtuals: true, versionKey: false },
  }
);

export default model<IPlace>('PlaceSchema', PlaceSchema);
