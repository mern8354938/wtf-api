import { Schema, model, Document, Types } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface IUser extends Document {
  name: string;
  email: string;
  password: string;
  image: string;
  places: Schema.Types.ObjectId[];
}

const UserSchema = new Schema<IUser>(
  {
    name: { required: true, type: String },
    email: { required: true, type: String, unique: true },
    password: { required: true, type: String, min: 6 },
    image: { required: true, type: String },
    places: [{ required: true, type: Types.ObjectId, ref: 'PlaceSchema' }],
  },
  { toObject: { virtuals: true, getters: true, versionKey: false } }
);
UserSchema.plugin(uniqueValidator);
export default model<IUser>('UserSchema', UserSchema);
