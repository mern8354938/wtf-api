import { placeUpdateValidator, placeValidator } from '../middleware/validator';
import checkAuth from '../middleware/check-auth';
const Router = require('express');
import {
  createPlace,
  deletePlace,
  getPlaceById,
  getPlacesByUserId,
  updatePlaceById,
} from '../controllers/places-controller';
import fileUpload from '../middleware/file-upload';

const router = Router();

router.use(checkAuth);

router
  .get('/:pid', getPlaceById)
  .get('/user/:uid', getPlacesByUserId)
  .post('/', fileUpload.single('image'), placeValidator(), createPlace)
  .patch(
    '/:pid',

    placeUpdateValidator(),
    updatePlaceById
  )
  .delete('/:pid', deletePlace);
export { router };
