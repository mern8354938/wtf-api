import { NextFunction, Router } from 'express';
import fileUpload from '../middleware/file-upload';
import {
  getAllUsers,
  loginUser,
  signUpUser,
} from '../controllers/users-controller';
import { userSignupValidator } from '../middleware/validator';

const router = Router();
router
  .get('/', getAllUsers)
  .post('/login', loginUser)
  .post(
    '/signup',
    fileUpload.single('image'),
    userSignupValidator(),
    signUpUser
  );

export { router };
