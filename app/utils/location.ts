import axios from 'axios';
import { HttpError } from '../modal/http-error';

const API_URL = 'https://nominatim.openstreetmap.org/search';

export const getCordsForAddress = async (address: string) => {
  try {
    const res = await axios.get(API_URL, {
      params: {
        q: address,
        format: 'json',
        limit: 1,
      },
    });
    const data = await res.data;
    if (data.length === 0) {
      return new HttpError(
        'could not find location for the specified address',
        422
      );
    }
    const { lat, lon } = data.at(0);
    return { lat, lon };
  } catch (e) {
    throw e;
  }
};
