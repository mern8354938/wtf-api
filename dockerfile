FROM alpine:3.17
RUN apk add --no-cache nodejs npm

WORKDIR /app
COPY package.json pnpm-lock.yaml ./
RUN npm install -g pnpm
RUN pnpm install

COPY . .

EXPOSE 5000
CMD ["pnpm", "dev"]
